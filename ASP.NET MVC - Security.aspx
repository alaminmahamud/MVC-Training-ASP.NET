ASP.NET MVC - Security

MVC Application Security
The Models Folder contains the classes that represent the application model.

Visual Web Developer automatically creates an AccountModels.cs file that contains the models for application authentication.

AccountModels contains a LogOnModel, a ChangePasswordModel, and a RegisterModel:


The Change Password Model
	
	public class ChangePasswordModel
	{

	[Required]
	[DataType(DataType.Password)]
	[Display(Name = "Current password")]
	public string OldPassword { get; set; }

	[Required]
	[StringLength(100, ErrorMessage = "The {0} must be at least {2}      characters long.", MinimumLength = 6)]
	[DataType(DataType.Password)]
	[Display(Name = "New password")]
	public string NewPassword { get; set; }

	[DataType(DataType.Password)]
	[Display(Name = "Confirm new password")]
	[Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
	public string ConfirmPassword { get; set; }

	}

