ASP.NET MVC - Styles and Layout

Adding Styles and a consistent look (Layout)

Adding a Layout

	The file _Layout.cshtml represent the layout of each page in the application. It is located in the Shared folder inside the Views folder.

	Open the file and swap the content with this:


	<!DOCTYPE html>
	<html>
	<head>
	<meta charset="utf-8" />
	<title>@ViewBag.Title</title>
	<link href="@Url.Content("~/Content/Site.css")" rel="stylesheet" type="text/css" />
	<script src="@Url.Content("~/Scripts/jquery-1.5.1.min.js")"></script>
	<script src="@Url.Content("~/Scripts/modernizr-1.7.min.js")"></script>
	</head>
	<body>
	<ul id="menu">
	<li>@Html.ActionLink("Home", "Index", "Home")</li>
	<li>@Html.ActionLink("Movies", "Index", "Movies")</li>
	<li>@Html.ActionLink("About", "About", "Home")</li>
	</ul> 
	<section id="main">
	@RenderBody()
	<p>Copyright W3schools 2012. All Rights Reserved.</p>
	</section>
	</body>
	</html>


HTML Helpers

	In the code above, HTML helpers are used to modify HTML output:

	@Url.Content() - URL content will be inserted here.

	@Html.ActionLink() - HTML link will be inserted here.

Razor Syntax
	
	@ViewBag.Title - The page title will be inserted here.

	@RenderBody() - The page content will be rendered here.

Adding Styles
	
	add some bunch of css 

The _ViewStart File

@{Layout = "~/Views/Shared/_Layout.cshtml";}