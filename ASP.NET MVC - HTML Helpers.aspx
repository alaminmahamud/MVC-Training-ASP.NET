ASP.NET MVC - HTML Helpers

Razor Syntax

@Html.ActionLink("About this website", "About")

ASP Syntax

<%=
	Html.ActionLink("About this website", "About")
%>

