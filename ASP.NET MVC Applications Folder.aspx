ASP.NET MVC Applications Folder

Exploring the Application Folder

MVC Folders
	
	Application Information
		Properties
		References

	Application Folders
		App_data Folder
		Content Folder
		Controllers Folder
		Models Folder
		Scripts Folder
		Views Folder

	Configuration Files
		Global.asax
		packages.config
		Web.config

The folder names are equal in all MVC applications. The MVC framework is based on default naming. Controllers are in the Controllers folder, Views are in the Views folder, and Models are in the Models folder. You don't have to use the folder names in your application code.

Standard naming reduces the amount of code, and makes it easier for developers to understand MVC projects.


The App_Data Folder
	
	The App_Data folder is for storing application data
	We will add an SQL database to the App_Data folder

The Content Folder
	
	The Content folder is used for static files like style sheets (css files), icons and images.

	Visual Web Developer automatically adds a themes folder to the Content folder. The themes folder is filled with jQuery styles and pictures. In this project you can delete the themes folder.

	Visual Web Developer also adds a standard style sheet file to the project: the file Site.css in the content folder. The style sheet file is the file to edit when you want to change the style of the application.

The Controllers Folder
	
	The Controllers folder contains the controller classes responsible for handling user input and responses.

	MVC requires the name of all controller files to end with "Controller".

	Visual Web Developer has created a Home controller (for the Home and the About page) and an Account controller (for Login pages):

The Models Folder
	
	The Models folder contains the classes that represent the application models. Models hold and manipulate application data.


The Views Folder
	
	The Views folder stores the HTML files related to the display of the application (the user interfaces).

	The Views folder contains one folder for each controller. 

	Visual Web Developer has created an Account folder, a Home folder, and a Shared folder (inside the Views folder).

	The Account folder contains pages for registering and logging in to user accounts.

	The Home folder is used for storing application pages like the home page and the about page.

	The Shared folder is used to store views shared between controllers (master pages and layout pages).

The Scripts Folder
	
	The Scripts folder stores the JavaScript files of the application.

	By default Visual Web Developer fills this folder with standard MVC, Ajax, and jQuery files

	Note: The files named "modernizr" are JavaScript files used for supporting HTML5 and CSS3 features in the application


