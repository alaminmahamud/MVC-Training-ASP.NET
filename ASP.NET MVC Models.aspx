ASP.NET MVC Models

MVC Models

	The MVC Model contains all application logic (business logic, validation logic, and data access logic), except pure view and controller logic.

	With MVC, models both hold and manipulate application data.

The Models Folder
	
	The Models Folder contains the classes that represent the application model.
	Visual Web Developer automatically creates an AccountModels.cs file that contains the models for application security.
	AccountModels contains a LogOnModel, a ChangePasswordModel, and a RegisterModel


Adding a Database Model

	The database model needed for this tutorial can be created with these simple steps:

	In the Solution Explorer, right-click the Models folder, and select Add and Class.
	Name the class MovieDB.cs, and click Add.
	Edit the class:

	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web;
	using System.Data.Entity;

	namespace MvcDemo.Models
	{
	public class MovieDB
	{
	public int ID { get; set; }
	public string Title { get; set; }
	public string Director { get; set; }
	public DateTime Date { get; set; }

	}
	public class MovieDBContext : DbContext
	{
	public DbSet<MovieDB> Movies { get; set; } 
	}
	}

We have deliberately named the model class "MovieDB". In the previous chapter, you saw the name "MovieDBs" (endig with s) used for the database table. It looks strange, but this is the naming convention you have to use to make the model connect to the database table.

Adding Database Controller

Adding Database Views


