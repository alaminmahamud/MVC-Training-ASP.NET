ASP.NET MVC - Views

	Adding Views for Displaying the Application

The Views Folder
	
	The Views folder stores the files (HTML files) related to the display of the application (the user interfaces). These files may have the extensions html, asp, aspx, cshtml, and vbhtml, depending on the language content.

	The Views folder contains one folder for each controller. 

	Visual Web Developer has created an Account folder, a Home folder, and a Shared folder (inside the Views folder).

	The Account folder contains pages for registering and logging in to user accounts.

	The Home folder is used for storing application pages like the home page and the about page.

	The Shared folder is used to store views shared between controllers (master pages and layout pages).

ASP.NET File Types

	The following HTML file types can be found in the Views Folder:

	File Type	Extention

	Plain HTML 			.htm or .html
	Classic ASP			.asp 
	Classic ASP.NET		.aspx
	ASP.NET Razor C#	.cshtml
	ASP.NET Razor VB	.vbhtml

The Index File
	
	The File Index.cshtml represents the HomePage of the Application.
	It is the Applications default file(index file)

	@{ViewBag.Title = "Home Page";}
	<h1>Welcome to W3Schools</h1>
	<p>---------------------</p>

The About File

	The file About.cshtml represent the About page of the application.

	Put the following content in the file:

	@{ViewBag.Title = "About Us";}

	<h1>About Us</h1>

	<p>Put About Us content here</p>


