ASP.NET MVC - Internet Application

What We Will Do
	
	Visual Web Developer offers different templates for building web applications.

	We will use Visual Web Developer to create an empty MVC Internet application with HTML5 markup.

	When the empty Internet application is created, we will gradually add code to the application until it is fully finished. We will use C# as the programming language, and the newest Razor server code markup.

	Along the way we will explain the content, the code, and all the components of the application