
ASP.NET MVC Controllers

Adding a Controller
	
	The Controllers Folder contains the controller classes responsible for handling user input and responses.

	MVC requires the name of all controllers to end with "Controller".

	In our example, Visual Web Developer has created the following files: HomeController.cs (for the Home and About pages) and AccountController.cs (For the Log On pages):


Web servers will normally map incoming URL requests directly to disk files on the server. For example: an URL request like "http://www.w3schools.com/default.asp" will map directly to the file "default.asp" at the root directory of the server.

The MVC framework maps differently. MVC maps URLs to methods. These methods are in classes called "Controllers".

Controllers are responsible for processing incoming requests, handling input, saving data, and sending a response to send back to the client.

The Home Controller

The controller file in our application HomeController.cs, defines the two controls Index and About.

Swap the content of the HomeController.cs file with this:

	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web; 
	using System.Web.MVC;

	namespace MVCDemo.Controllers
	{
		public class HomeController : Controller
		{
			public ActionResult Index()
			{
				return View();
			}

			public ActionResult About()
			{
				return View();
			}
		}
	}


	The Controller Views

	The file Index.cshtml and About.cshtml in the views folder defines the ActionResult views Index() and About() in the controller.
	